package com.example.myrecyclerviewlp21;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.example.myrecyclerviewlp21.Tools.MyAdapter;
import com.example.myrecyclerviewlp21.Tools.MyViewHolder;
import com.example.myrecyclerviewlp21.models.Produit;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Produit> produits = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MyAdapter myAdapter;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        if (sharedPreferences.contains("lesproduits")){
            produits = new Gson().fromJson(
                    sharedPreferences.getString("lesproduits",null),
                    new TypeToken<List<Produit>>(){}.getType());
        }else{
            construireListeProduits(10);
        }
        myAdapter = new MyAdapter(produits);
        recyclerView.setAdapter(myAdapter);
    }


    private void construireListeProduits(int nb){
        for(int i=0;i<nb;i++){
            produits.add(new Produit("Produit "+(i+1),(i+1)));
        }
    }

    public void ajouter(View view) {
        Intent intent =new Intent(this,MainActivity2.class);
        launchSomeActivity.launch(intent);
        //startActivityForResult(intent,1);
    }

    ActivityResultLauncher<Intent> launchSomeActivity = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    Produit produit = data.getParcelableExtra("produit");
                    produits.add(produit);
                    myAdapter.notifyItemInserted(produits.size()-1);
                }
            });

    @Override
    protected void onPause(){
        super.onPause();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("lesproduits",new Gson().toJson(produits)).apply();
    }

}