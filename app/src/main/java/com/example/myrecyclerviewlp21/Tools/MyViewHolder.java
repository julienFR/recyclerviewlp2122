package com.example.myrecyclerviewlp21.Tools;

import android.app.AlertDialog;
import android.view.View;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.example.myrecyclerviewlp21.R;
import com.example.myrecyclerviewlp21.models.Produit;

public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
    private TextView tvNom;
    private TextView tvQte;
    private Produit produit;
    private ItemLongClickListener itemLongClickListener;

    public MyViewHolder(View itemView) {
        super(itemView);
        tvNom = itemView.findViewById(R.id.tvNomProd);
        tvQte = itemView.findViewById(R.id.tvQte);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(v.getContext())
                        .setTitle(produit.getNom())
                        .setMessage(produit.getQt().toString())
                        .show();
            }
        });
        itemView.setOnLongClickListener(this);
    }

    public void display(Produit p){
        tvNom.setText(p.getNom());
        tvQte.setText("Quantite : " + p.getQt().toString());
        produit = p;
    }

    @Override
    public boolean onLongClick(View v) {
        this.itemLongClickListener.onItemLongClick(v,getLayoutPosition());
        return false;
    }

    public void setItemLongClickListener(ItemLongClickListener itemLongClickListener){
        this.itemLongClickListener = itemLongClickListener;
    }
}
