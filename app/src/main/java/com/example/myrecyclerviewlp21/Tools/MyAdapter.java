package com.example.myrecyclerviewlp21.Tools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.recyclerview.widget.RecyclerView;

import com.example.myrecyclerviewlp21.R;
import com.example.myrecyclerviewlp21.models.Produit;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private static ArrayList<Produit> produits;

    public MyAdapter(ArrayList<Produit> p){
        produits = p;
    }

    @Override
    //permet de creer des nouvelles vues pour la recyclerview
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.layout_produit,parent,false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    //remplacer ou fournir le contenu d'une vue
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Produit produit = produits.get(position);
        holder.display(produit);
        holder.setItemLongClickListener(new ItemLongClickListener() {
            @Override
            public void onItemLongClick(View v, int pos) {
                produits.remove(pos);
                notifyItemRemoved(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return produits.size();
    }
}
