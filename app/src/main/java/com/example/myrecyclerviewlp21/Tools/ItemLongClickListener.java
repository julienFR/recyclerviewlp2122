package com.example.myrecyclerviewlp21.Tools;

import android.view.View;

public interface ItemLongClickListener {
    void onItemLongClick(View v, int pos);
}
