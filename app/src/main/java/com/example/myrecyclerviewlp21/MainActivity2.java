package com.example.myrecyclerviewlp21;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.myrecyclerviewlp21.models.Produit;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    @Override
    public void finish(){
        EditText editTextNom = findViewById(R.id.editTextTextNom);
        EditText editTextQt = findViewById(R.id.editTextQuantite);
        Produit produit = new Produit(editTextNom.getText().toString(),
                Integer.parseInt(editTextQt.getText().toString()));
        Intent intent = new Intent();
        intent.putExtra("produit",produit);
        setResult(Activity.RESULT_OK,intent);
        super.finish();
    }

    public void backtoMain(View view) {
        finish();
    }
}