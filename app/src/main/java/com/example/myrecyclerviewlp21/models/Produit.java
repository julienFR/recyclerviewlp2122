package com.example.myrecyclerviewlp21.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Produit implements Parcelable {
    private String nom;
    private Integer qt;


    public Produit(String nom, Integer qt) {
        this.nom = nom;
        this.qt = qt;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getQt() {
        return qt;
    }

    public void setQt(Integer qt) {
        this.qt = qt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeInt(qt);
    }

    public Produit(Parcel parcel){
        this.nom = parcel.readString();
        this.qt = parcel.readInt();
    }

    public static  final Creator<Produit> CREATOR = new Creator<Produit>() {
        @Override
        public Produit createFromParcel(Parcel source) {
            return new Produit(source);
        }

        @Override
        public Produit[] newArray(int size) {
            return new Produit[size];
        }
    };

}
